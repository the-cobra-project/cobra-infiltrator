NAME:
  Fetch Device Info v1.2
VERSION:
  1.2
SYNOPSIS:
  fetch-dev
DEVELOPER:
  Cobra Technology Labs
LOCATION:
  /sysbin/fetch-dev.bin
DESCRIPTION:
  Fetch Device Info is a tool for grabbing key information about a host OS.
  
EOF >> echo fetch-dev.man