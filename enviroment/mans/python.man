NAME:
  Python 3 Infiltrator Adaption
VERSION:
  3.x
SYNOPSIS:
  python <file>, python
DEVELOPER:
  Cobra Technology Labs x Python
LOCATION:
  /sysbin/CIpython3.bin
DESCRIPTION:
  The Python 3 Interpreter adapted for Cobra Infiltrator

EOF >> echo python.man