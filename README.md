# Cobra Infiltrator

Cobra Infiltrator is a simple but powerfull hacking and exploitation Operating System, Written a mix of bash and python 3, Including many popular and unknown tools.

## Installation

Type the below into a terminal window.

```bash
wget -O install-infiltrator https://gitlab.com/the-cobra-project/cobra-infiltrator/raw/master/installer/installer.py
```
Then type ```chmod +x install-infiltrator``` into your terminal followed by ```./install-infiltrator``` to start the installation, This shouldn't take long.

## Launching Cobra Infiltrator

In the same working directory type ```./start-infiltrator.sh``` The installed version of Cobra Infiltrator should boot if installed correctly.
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to test as appropriate.
[TE - Testing Enviroment](https://repl.it/@CobraInfltrtr/TE)

## License
Cobra Infiltrator is licensed under a [MIT](https://choosealicense.com/licenses/mit/) License and thus is Open-Source.
